# CONTRIBUTING

## Getting started

Requirements are listed in `setup.cfg` and can be installed with

```bash
pip install [--user] .[dev]
```

## Linting

The code is formatted with [black](https://black.readthedocs.io/en/stable/) and linted by [flake8](https://flake8.pycqa.org/en/latest/index.html). The repository also contains notebooks that are linted with [flake8-nb](https://flake8-nb.readthedocs.io/en/latest/).

The configuration can be found in `setup.cfg`.

## Testing

Tests make use [pytest](https://docs.pytest.org/en/stable/index.html) and can be run as follows

```bash
pytest .
```

## Write documentation

The documentation is composed of RST files located in `doc`. You can look at the [Sphinx doc](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html) for information on how to write RST files.

If a new file is created, don't forget to reference it in one of the `toctree` directive.

## Build documentation

The documentation is built with [Sphinx](https://www.sphinx-doc.org/en/master/) that generates HTML pages out of the RST files. The configuration of Sphinx is in `doc/conf.py`.

Requirements (including Sphinx) can be installed with

```bash
pip install [--user] .[doc]
```

Then, build the documentation with

```bash
sphinx-build doc build/sphinx/html -E -a
```

The generated HTML pages will be available in `build/sphinx/html`. You can browse them easily by opening `build/sphinx/html/index.html` in your browser.

When rebuilding the documentation, don't forget to remove generated files to have a fresh `autodoc` documentation:

```bash
rm -rf doc/_generated/; sphinx-build doc build/sphinx/html -E -a
```
