User Guide
==========

.. toctree::
  :maxdepth: 1
  :hidden:

  install.rst
  larch_pymca.rst
  widgets/index.rst


First steps
"""""""""""

:doc:`install`

Advanced documentation
""""""""""""""""""""""

:doc:`larch_pymca`

:doc:`widgets/index`

