Installation
::::::::::::

est is library made to link several libraries together and define treatment workflows.

This is the recommended way to install est.

Step 1 - Create a virtual env
'''''''''''''''''''''''''''''

It is recommended to create a python virtual environment to run the workflow tool.
Virtual environment might avoid some conflict between python packages. But you can also install it on your 'current' python environment and move to step 1.

.. code-block:: bash

   virtualenv --python=python3 --system-site-packages myvirtualenv


Then activate the virtual environment

.. code-block:: bash

   source myvirtualenv/bin/activate

.. note:: To quit the virtual environment

   .. code-block:: bash

      deactivate

Step 2 installation
'''''''''''''''''''

clone the git repository ( and the submodule - --recursive option)

git clone --recursive https://gitlab.esrf.fr/workflow/est.git

You can install from the code source (https://gitlab.esrf.fr/workflow/est)

.. code-block:: bash

   pip install .[full]

.. note:: Has explained in the description the goal of est is to link pymca and / or larch treatments to define a workflow. By default neither pymca or larch are installed. To use est you should have at least one installed.

Orange3 add-on
::::::::::::::

est is offering an orange add-on for having a convenient and user friendly way to define a treatment workflow.
To benefit from the orange add-on you will need to have orange3 canvas installed.

.. code-block:: bash

   pip install orange3


To launch orange and access the est add-on you can simply launch Orange2 with the command:

.. code-block:: bash

   orange-canvas

The new widgets appears in the toolbox bar under the section Example.

.. image:: img/workflow_example.png

You can find in /doc/tutorials examples of simple workflow definition using pymca and larch treatments. It can be a good starting point.


.. note:: if your installed a virtual environment do not forget to active it :

.. code-block:: bash

   source myvirtualenv/bin/activate


Documentation
:::::::::::::

.. code-block:: bash

   cd doc
   make html

The documentation is build in doc/build/html and the entry point is index.html

.. code-block:: bash

   firefox build/html/index.html

.. note:: the build of the documentation need sphinx to be installed. This is not an hard dependency. So you might need to install it.


You also should generate documentation to be accessible from Orange GUI (pressing the F1 key).

.. code-block:: bash

   python setup build build_sphinx -b htmlhelp


Warning
:::::::

At the time being (August 2019) the est GUI is using silx from the silx.gui.qt module which try to use PyQt5, then PySide2, then PyQt4
Order is different for AnyQt, used by Orange3. So some incoherence might append (will bring errors) in the Signal/SLOT connection
during widget instantiation.

You should be careful about it if more than one Qt binding is available (one quick and durty fix is to change the AnyQt/__init__.py file - 'availableapi' function for example)
