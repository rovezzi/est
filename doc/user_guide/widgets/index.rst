
Widgets
-------

.. the orange widgets section has to be stored here to fit the orange requirement (and accessing the help from orange-canvas)

This is the list of generic util Orange widget:

.. toctree::
  :maxdepth: 1

  xas_converter.rst
  xas_roi.rst
  xas_energy_roi.rst
  xas_input.rst
  xas_output.rst

Here the list of Pymca process available from the Orange add-on:

.. toctree::
  :maxdepth: 1

  pymca/exafs.rst
  pymca/ft.rst
  pymca/k_weight.rst
  pymca/normalization.rst

Here the list of Larch process available from the Orange add-on:

.. toctree::
  :maxdepth: 1

  larch/autobk.rst
  larch/mback.rst
  larch/mback_norm.rst
  larch/pre_edge.rst
  larch/xftf.rst


List of generic processes:

.. toctree::
  :maxdepth: 1

  noise.rst
