autobk
======

Background subtraction, convert mu to :math:`\chi(k)` for quantitative analysis (post_edge background function)