pre edge
========

Pre-edge subtraction and normalization:

    - fit a line of polynomial to the region below the edge
    - determine E0
    - fit a polynomial to the region above the edge
    - extrapolate the two curves to E0 to determine the edge

note: you can open a pop up window for computing mean or median E0 value from the dataset using the button at the right of the E0 entry line