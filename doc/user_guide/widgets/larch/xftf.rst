xftf
====

Forward XAFS Fourier transforms forward Fourier transform converts :math:`\chi(k)` to :math:`\chi(R)` and is of primary importance for XAFS analysis.