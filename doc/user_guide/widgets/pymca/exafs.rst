exafs
=====

Post-edge Background Subtraction, convert mu to :math:`\chi(k)` / EXAFSValues for quantitative analysis

.. image:: ../img/pymca_exafs.png
