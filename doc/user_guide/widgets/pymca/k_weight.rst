k weight
========

define k weight to set the k weight parameter of larch xftf and pymca ft

.. image:: ../img/pymca_kweight.png
