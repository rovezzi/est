normalization
=============

Spectra normalization.

.. image:: ../img/pymca_normalization.png

note: you can open a pop up window for computing mean or median E0 value from the dataset using the button at the right of the E0 entry line