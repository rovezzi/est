converter xas_obj -> Table
==========================

Convert a xas_obj to a Orange.data.Table
It will only convert energy and absorption beam spectra.
Other information (treatment results, process flow...) will be lost.
