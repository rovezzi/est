Energy ROI
==========

Define the energy range to be analysed.

.. image:: img/energy_roi.png
