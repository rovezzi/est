xas input
=========

Use to load a spectra from a .dat file or .h5 file

.. image:: img/xas_input.png
