from urllib.request import urlopen
from pathlib import Path
from est import resources


RESOURCE_ROOT = Path(__file__).resolve().parent.parent / "est" / "resources"


def download(url: str, filename: Path):
    with urlopen(url) as response:
        with open(filename, "wb") as out_file:
            out_file.write(response.read())
    assert filename.exists()
    print("SAVED:", filename)


def download_larch_cu():
    url = "https://raw.githubusercontent.com/xraypy/xraylarch/master/examples/xafs/cu_rt01.xmu"
    filename = RESOURCE_ROOT / "exafs" / "cu_rt01.xmu"
    download(url, filename)
    resources.generate_resource("exafs/cu_rt01.xmu", overwrite=True)


def download_pymca_cu():
    url = "https://raw.githubusercontent.com/vasole/pymca/master/PyMca5/PyMcaData/EXAFS_Cu.dat"
    filename = RESOURCE_ROOT / "exafs" / "EXAFS_Cu.dat"
    download(url, filename)
    resources.generate_resource("exafs/EXAFS_Cu.dat", overwrite=True)


def download_pymca_ge():
    url = "https://raw.githubusercontent.com/vasole/pymca/master/PyMca5/PyMcaData/EXAFS_Ge.dat"
    filename = RESOURCE_ROOT / "exafs" / "EXAFS_Ge.dat"
    download(url, filename)
    resources.generate_resource("exafs/EXAFS_Ge.dat", overwrite=True)


if __name__ == "__main__":
    download_larch_cu()
    download_pymca_cu()
    download_pymca_ge()
