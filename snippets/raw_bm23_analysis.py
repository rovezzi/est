from silx.io.url import DataUrl
from est.core.io import read_from_input_information
from est.units import ur
from larch.xafs import etok
from silx.gui import qt
from silx.gui.plot import Plot1D
from est.io.information import InputInformation
from est.thirdparty.silx.hdf5file import HDF5File, get_data
import logging
import threading
import time
from silx.gui.utils import concurrent
from est.core.process.noise import NoiseProcess
from silx.io import utils as silx_utils

_logger = logging.getLogger(__name__)

HAS_REACH_END_VALUE = True


class AnalysisThread(threading.Thread):
    """Thread updating the curve of a :class:`~silx.gui.plot.Plot1D`
    :param plot1d: The Plot1D to update."""

    TIME_BETWEEN_ANALYSIS = 1

    def __init__(self, url, widget):
        self.url = url
        self.widget = widget
        self.spectrum = None
        self.running = False
        self._first_plot = True
        super().__init__()

    def start(self):
        """Start the update thread"""
        self.running = True
        self.running = True
        super().start()

    def run(self):
        """Method implementing thread loop that updates the plot"""
        while self.running:
            time.sleep(self.TIME_BETWEEN_ANALYSIS)
            self.do_analysis()
            # Run plot update asynchronously
            self.plot()

    def stop(self):
        """Stop the update thread"""
        self.running = False
        self.join(2)

    def get_configuration(self):
        self.auto_bk_kweight = 3
        self.xftf_kweight = 2
        max_raw_energy = max(self.spectrum.energy)
        if HAS_REACH_END_VALUE:
            norm2 = max_raw_energy - 100
        else:
            norm2 = max_raw_energy
        ft_k_range_end = etok(norm2)  # here ft_k value seems really strange no ?

        return {
            "pre_edge": {
                "e0": None,
                "step": None,
                "pre1": -150.0,
                "pre2": -30.0,
                "norm1": 100.0,
                "norm2": "E_end -100",
                "nnorm": 2,
                "nvict": 0,
                "make_flat": True,
                "emin_area": None,
            },
            "noise": {
                "polynomial_order": 2,
                "window_size": 5,
                "e_min": 100,
                "e_max": max_raw_energy - 10,
            },
            "autobk": {
                "rbkg": 1,
                "e0": None,
                "edge_step": None,
                "kmin": 0,
                "kmax": None,
                "kweight": self.auto_bk_kweight,
                "dk": 0.1,
                "win": "hanning",
                "nfft": 2048,
                "kstep": 0.05,
                "nclamp": 3,
                "clamp_lo": 0,
                "clamp_hi": 1,
                "calc_uncertaintites": True,
                "err_sigma": 1,
                "nknots": None,
            },
            "xftf": {
                "rmax_out": 10,
                "kweight": self.xftf_kweight,
                "kmin": 3,
                "kmax": ft_k_range_end,
                "dk": 1,
                "dk2": None,
                "window": "hanning",
                "nfft": 2048,
                "kstep": 0.05,
                "with_phase": True,
            },
        }

    def do_analysis(self):
        channel_url = DataUrl(
            file_path=self.url.file_path(),
            data_path=self.url.data_path() + "/measurement/musst_eneenc",
            scheme="silx",
        )
        spectra_url = DataUrl(
            file_path=self.url.file_path(),
            data_path=self.url.data_path() + "/measurement/mu_trans",
            scheme="silx",
        )
        self.I0_url = DataUrl(
            file_path=self.url.file_path(),
            data_path=self.url.data_path() + "/measurement/I0",
            scheme="silx",
        )
        self.I1_url = DataUrl(
            file_path=self.url.file_path(),
            data_path=self.url.data_path() + "/measurement/I1",
            scheme="silx",
        )
        self.I2_url = DataUrl(
            file_path=self.url.file_path(),
            data_path=self.url.data_path() + "/measurement/I2",
            scheme="silx",
        )
        input_information = InputInformation(
            spectra_url=spectra_url,
            channel_url=channel_url,
            energy_unit=ur.keV,
        )

        # set I0, I1 and I2 if exists
        for url, set_fct in zip(
            (self.I0_url, self.I1_url, self.I2_url), ("I0_url", "I1_url", "I2_url")
        ):
            try:
                with HDF5File(filename=url.file_path(), mode="r") as h5f:
                    if url.data_path() in h5f:
                        setattr(input_information, set_fct, url)
            except Exception:
                _logger.warning("{} not set because not found".format(set_fct))

        self.mu_ref_url = DataUrl(
            file_path=self.url.file_path(),
            data_path=self.url.data_path() + "/measurement/mu_ref",
            scheme="silx",
        )

        # this could be replace by a larch Spectrum. We would just have to duplicated noise treatment
        xas_obj = read_from_input_information(input_information)
        self.spectrum = xas_obj.spectra.data.flat[0]

        # TODO: add a process attact to attach informations from an url.
        try:
            xas_obj.spectra.data.flat[0].I0 = get_data(self.I0_url)
        except Exception:
            pass
        try:
            xas_obj.spectra.data.flat[0].I1 = get_data(self.I1_url)
        except Exception:
            pass
        try:
            xas_obj.spectra.data.flat[0].I2 = get_data(self.I2_url)
        except Exception:
            pass
        try:
            xas_obj.spectra.data.flat[0].mu_ref = get_data(self.mu_ref_url)
        except Exception:
            pass

        configuration = self.get_configuration()
        # compute pre_edge
        import est.core.process.larch.pre_edge

        pre_edge_process = est.core.process.larch.pre_edge.Larch_pre_edge()
        pre_edge_process.set_properties(configuration)
        pre_edge_process.process(xas_obj)

        # compute autobk (should be updated)
        import est.core.process.larch.autobk

        autobk_process = est.core.process.larch.autobk.Larch_autobk()
        autobk_process.set_properties(configuration)
        autobk_process.process(xas_obj)

        # compute xftf
        import est.core.process.larch.xftf

        xftf_process = est.core.process.larch.xftf.Larch_xftf()
        xftf_process.set_properties(configuration)
        xftf_process.process(xas_obj)

        # compute noise
        # for this one we have to "mask extrem values"

        noise_process = NoiseProcess()
        noise_process.set_properties(configuration)
        noise_process.process(xas_obj)

        # dump results to h5 file
        xas_obj.spectra.data.flat[0] = self.spectrum

    def plot(self):
        self.update_plot_1()
        self.update_plot_2()
        self.update_plot_3()
        self.update_plot_4()
        self._first_plot = False

    def update_plot_1(self):
        # raw data
        concurrent.submitToQtMainThread(
            self.widget.plot1.addCurve,
            x=self.spectrum.energy,
            y=self.spectrum.flat,
            legend="flat normalized mu",
        )
        concurrent.submitToQtMainThread(
            self.widget.plot1.addCurve,
            x=self.spectrum.energy,
            y=self.spectrum.mu,
            legend="raw mu",
        )
        concurrent.submitToQtMainThread(
            self.widget.plot1.addCurve,
            x=self.spectrum.energy,
            y=self.spectrum.normalized_mu,
            legend="normalized mu",
        )

        # ref data
        try:
            mu_ref = get_data(self.mu_ref_url)
        except Exception:
            pass
        else:
            concurrent.submitToQtMainThread(
                self.widget.plot1.addCurve,
                x=self.spectrum.energy,
                y=mu_ref,
                legend="mu ref",
            )

        # I0, I1, I2
        for IX_url, IX_legend in zip(
            (self.I0_url, self.I1_url, self.I2_url), ("I0", "I1", "I2")
        ):
            try:
                I_data = silx_utils.get_data(IX_url)
            except Exception:
                pass
            else:
                concurrent.submitToQtMainThread(
                    self.widget.plot1.addCurve,
                    x=self.spectrum.energy,
                    y=I_data,
                    legend=IX_legend,
                    yaxis="right",
                )
        if self._first_plot:
            # you can select the curve to display from plot > options > legend
            for legend in ("I0", "I1", "I2", "mu ref", "raw mu", "normalized mu"):
                concurrent.submitToQtMainThread(self.widget.plot1.hideCurve, legend)

    def update_plot_2(self):
        mask = self.spectrum.k < 16
        concurrent.submitToQtMainThread(
            self.widget.plot2.addCurve,
            x=self.spectrum.k[mask],
            y=self.spectrum.chi[mask] * (self.spectrum.k[mask] ** self.xftf_kweight),
            legend="chi",
        )

    def update_plot_3(self):
        concurrent.submitToQtMainThread(
            self.widget.plot3.addCurve,
            x=self.spectrum.r,
            y=self.spectrum.chir_mag,
            legend="mean noise_savgol",
        )

    def update_plot_4(self):
        concurrent.submitToQtMainThread(
            self.widget.plot4.addCurve,
            x=self.spectrum.noise_savgol_energy,
            y=self.spectrum.noise_savgol,
            legend="noise_savgol",
        )
        concurrent.submitToQtMainThread(
            self.widget.plot4.addYMarker,
            y=self.spectrum.norm_noise_savgol,
            legend="mean noise_savgol",
            color="red",
        )


def main():
    import argparse
    import sys

    # handle input parameters
    parser = argparse.ArgumentParser(description=__doc__)
    # single file input option
    parser.add_argument(
        "hdf5_file",
        help="Input .hdf5 bliss file. Example: Cu_CHA_68E_Proc5b.h5",
    )
    parser.add_argument(
        "entry",
        help="Entry (scan) to treat. Example: '110.1'",
    )
    options = parser.parse_args(sys.argv[1:])
    hdf5_file = options.hdf5_file
    entry = options.entry

    # handle GUI

    global app
    app = qt.QApplication([])
    widget = qt.QWidget()

    # raw data
    plot1 = Plot1D(widget)
    plot1.setWindowTitle("raw data")
    plot1.setGraphGrid(True)
    plot1.getXAxis().setLabel("Energy (eV)")
    plot1.getYAxis().setLabel("Absorption")

    plot2 = Plot1D(widget)
    plot2.setWindowTitle("chi")
    plot2.setGraphGrid(True)
    plot2.getXAxis().setLabel("Wavenumber (Anstrom^-1)")
    plot2.getYAxis().setLabel("k2X(k) (Anstrom^-2)")

    plot3 = Plot1D(widget)
    plot3.setWindowTitle("FT")
    plot3.setGraphGrid(True)
    plot3.getXAxis().setLabel("Radial distance (Angstom)")
    plot3.getYAxis().setLabel("chi(k) magnitude")

    plot4 = Plot1D(widget)
    plot4.setWindowTitle("noise")
    plot4.setYAxisLogarithmic(True)
    plot4.getXAxis().setLabel("energy")
    plot4.getYAxis().setLabel("noise")

    widget.setLayout(qt.QGridLayout())
    widget.layout().addWidget(plot1, 0, 0, 1, 1)
    widget.layout().addWidget(plot2, 0, 1, 1, 1)
    widget.layout().addWidget(plot3, 1, 0, 1, 1)
    widget.layout().addWidget(plot4, 1, 1, 1, 1)
    widget.plot1 = plot1
    widget.plot2 = plot2
    widget.plot3 = plot3
    widget.plot4 = plot4
    widget.show()

    url = DataUrl(file_path=hdf5_file, data_path=entry, scheme="silx")
    updateThread = AnalysisThread(url=url, widget=widget)
    updateThread.start()  # Start updating the plot

    app.exec_()

    updateThread.stop()  # Stop updating the plot


if __name__ == "__main__":
    # example of usage: python raw_bm_analysis.py Cu_CHA_68E_Proc5b.h5 110.1
    main()
