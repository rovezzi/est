import numpy
import pytest

try:
    import larch
except ImportError:
    larch = None
else:
    from est.core.process.larch.xftf import process_spectr_xftf as xftf
    from est.core.process.larch.autobk import process_spectr_autobk as autobk
    from est.core.process.larch.pre_edge import process_spectr_pre_edge as pre_edge


@pytest.mark.skipif(larch is None, reason="xraylarch not installed")
def testAutobk(spectrum_cu_from_larch):
    """equivalent treatment as the 'xafs.autobk.par script from larch'"""
    pre_edge(
        spectrum=spectrum_cu_from_larch,
        overwrite=True,
        configuration={"rbkg": 1.0, "kweight": 2},
    )
    autobk(
        spectrum=spectrum_cu_from_larch,
        overwrite=True,
        configuration={
            "kmin": 2,
            "kmax": 16,
            "dk": 3,
            "window": "hanning",
            "kweight": 2,
        },
    )
    xftf(spectrum=spectrum_cu_from_larch, overwrite=True, configuration={"kweight": 2})


@pytest.mark.skipif(larch is None, reason="xraylarch not installed")
def testXafsft1(spectrum_cu_from_larch):
    """equivalent treatment as the 'xafs.doc_xafs1.par script from larch'"""
    autobk(
        spectrum=spectrum_cu_from_larch,
        overwrite=True,
        configuration={"rbkg": 1.0, "kweight": 2, "clamp+hi": 10},
    )
    assert spectrum_cu_from_larch.k is not None
    assert spectrum_cu_from_larch.chi is not None
    conf, spec_dk1 = xftf(
        spectrum=spectrum_cu_from_larch,
        overwrite=False,
        configuration={
            "kweight": 2,
            "kmin": 3,
            "kmax": 13,
            "window": "hanning",
            "dk": 1,
        },
    )
    conf, spec_dk2 = xftf(
        spectrum=spectrum_cu_from_larch,
        overwrite=False,
        configuration={
            "kweight": 2,
            "kmin": 3,
            "kmax": 13,
            "window": "hanning",
            "dk": 5,
        },
    )
    assert spec_dk1 != spec_dk2
    assert not numpy.array_equal(spec_dk1.chir_re, spec_dk2.chir_re)
