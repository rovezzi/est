NAME = "xas_larch"

ID = "orangecontrib.est.widgets.larch"

DESCRIPTION = "larch orange widget"

LONG_DESCRIPTION = "larch exafs processes"

ICON = "icons/larch.png"

BACKGROUND = "light-green"

PRIORITY = 3
