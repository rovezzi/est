NAME = "xas_utils"

ID = "orangecontrib.est.widgets.utils"

DESCRIPTION = "common utils for xas"

LONG_DESCRIPTION = "common tools compatible with pymca and larch add-on"

ICON = "icons/xas.svg"

BACKGROUND = "light-blue"

PRIORITY = 2
